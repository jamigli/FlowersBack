﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace FlowersBack.Models
{
    public class DataContext : DbContext
    {
        public DataContext() : base("ConnectionDefault")
        {

        }

        public DbSet<Flower> Flowers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}